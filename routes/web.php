<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/


Route::get('/login', function () {
    return view('auth.login');
});

Route::post('/sesion', 'controladorLogin@login')->name('login');;
Route::get('/logout', 'controladorLogin@logout')->name('logout');

session_start();

Route::group(['middleware' => 'sesion'], function () {

	Route::get('/', 'HomeController@index')->name('home');

	Route::get('user/index', 'UserController@index')->name('user.index');
	Route::post('user/buscar', 'UserController@buscar')->name('user.buscar');
	Route::post('user/store', 'UserController@store')->name('user.store');
	Route::get('user/bitacora', 'UserController@bitacora')->name('user.bitacora');
	Route::get('activos', 'FreeipaController@activos')->name('activos');

	Route::group(['middleware' => 'dc'], function () {
		Route::get('datacenter/show', 'DataCenterController@show')->name('datacenter.show');
		Route::get('datacenter/correo/{id}', 'DataCenterController@correo')->name('datacenter.correo');
		Route::put('datacenter/update/{id}', 'DataCenterController@update')->name('datacenter.update');
	});
	Route::group(['middleware' => 'rrhh'], function () {
		/*RUTAS FREEIPA CONTROLLER*/ 
		Route::get('freeipa/create', 'FreeipaController@create')->name('freeipa.create');
		Route::get('freeipa/search', 'FreeipaController@search')->name('freeipa.search');
		Route::get('freeipa/edit/{cedula}', 'FreeipaController@edit')->name('freeipa.edit');
		Route::put('freeipa/update/{cedula}', 'FreeipaController@update')->name('freeipa.update');
		Route::get('freeipa/disable/{cedula}', 'FreeipaController@disable')->name('freeipa.disable');
		Route::get('freeipa/email/{cedula}', 'FreeipaController@email')->name('freeipa.email');
		Route::post('freeipa/searchUser', 'FreeipaController@searchUser')->name('freeipa.searchUser');
		Route::post('freeipa/store', 'FreeipaController@store')->name('freeipa.store');
	});	
});

