<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Person;
use App\Bitacora;
use App\Http\Controllers\freeipa\Freeipa;

class DataCenterController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        //
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    public function show()
    {
        $personal = Person::where('correo', null)->get();
        return view('pages.datacenter.show', compact('personal'));
    }

    public function correo($id)
    {
        $correo = Person::find($id);

        return view('pages.datacenter.ficha', compact('correo'));
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $consulta = Person::find($id);

        $cedula = $consulta->cedula;
        $nombres = $consulta->nombre;
        $apellidos = $consulta->apellido;
        $usuario = $request->correo;
        $cargo = $consulta->cargo;
        $tipo = $consulta->tipoempleado;
        $direccion = $consulta->address;
        $fecha = $consulta->fecha;
        $ext = $consulta->ext;
        $mail = $request->correo.'@vicepresidencia.gob.ve';

        $ipa = Freeipa::Consultas();
        $r = $ipa->user()->findBy('uid', $usuario);
        $ced = $ipa->user()->findBy('employeenumber', $cedula);

        if ($ced) {
            return response()->json(['message'=>'¡La cedula ya se encuentra registrada!'], 500);
        }

        if($r){
            return response()->json(['message'=>'¡El correo ya existe, intente de nuevo con otro!'], 500);

        }
        else{

            $personal = Person::find($id);
            $personal->delete();

            $new_user_data = array(
            'employeenumber'  => $cedula,
            'givenname'       => $nombres,
            'sn'              => $apellidos,
            'uid'             => $usuario,
            'title'           => $cargo,
            'employeetype'    => $tipo,
            'carlicense'      => $fecha,
            'telephonenumber' => $ext,
            'userpassword'    => $cedula,
            'mail'    => $mail,
            );


            try {
                $add_user = $ipa->user()->add($new_user_data);
                if ($add_user) {
                    try {
                    
                        $add_user_group = $ipa->group()->addMember($direccion, $usuario);
                        if ($add_group) {
                            
                            
                        } else {
                            return response()->json(['message'=>'No se pudo registrar el grupo'], 500);
                        }
                    } catch (\Exception $e) {

                    }

                    $bitacora = new Bitacora;
                    $bitacora->user_end = $usuario;
                    $bitacora->user_id = $_SESSION['id'];
                    $bitacora->action_id = 3;
                    $bitacora->save(); 

                    return response()->json(['message' => 'Al empleado <b>'.$nombres.' '.$apellidos.'</b> se ha creado exitosamente su correo']);

                } else {
                    echo '¡Error!';
                    return response()->json(['message'=>'No se pudo crear el usuario'], 500);
                }
            } catch (\Exception $e) {
                return response()->json(['message'=>'No se pudo crear el usuario'], 500);

        }
            }
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }
}
