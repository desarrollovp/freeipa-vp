<?php
namespace App\Http\Controllers\freeipa;

use Illuminate\Support\Facades\Crypt;

class Freeipa {

	public static function Consultas()
	{
		require_once ('bootstrap.php');
		$host        = 'freeipa.vicepresidencia.gob.ve';
		$certificate = __DIR__ ."/certs/ipa.demo1.freeipa.org_ca.crt";
		$ipa         = new \FreeIPA\APIAccess\Main($host, $certificate);
		$user     = 'consultasdev';
		$password = 'DesarrolloVp4321';
		$auth     = $ipa->connection()->authenticate($user, $password);		

		return $ipa;
	}

	public static function Login($user, $clave)
	{
		$pass = Crypt::decrypt($clave);

		require_once ('bootstrap.php');
		$host        = 'freeipa.vicepresidencia.gob.ve';
		$certificate = __DIR__ ."/certs/ipa.demo1.freeipa.org_ca.crt";
		$ipa         = new \FreeIPA\APIAccess\Main($host, $certificate);
		$sesion = $ipa->connection()->authenticate($user, $pass);

		if ($sesion) {
			return $ipa;
		}
	}
}