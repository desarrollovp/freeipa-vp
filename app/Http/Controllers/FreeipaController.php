<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\User;
use App\Address;
use App\Bitacora;
use App\Person;
use App\TipoEmpleado;
use App\Roles;
use App\Cargo;
use DateTime;
use Illuminate\Support\Str;
use App\Http\Controllers\freeipa\Freeipa;

class FreeipaController extends Controller
{
    
    public function index()
    {
        //
    }

    public function activos()
    {
        $ipa = Freeipa::Consultas();
        $all = $ipa->user()->find();
        $count = 0;
        $data = [];
        for ($i = 0; $i < count($all); $i++) {
            if (count($all[$i]->memberof_group) < 2) {
                    array_push(
                        $data, 
                        array(
                            "uid" => $all[$i]->uid[0],
                            "cn" => $all[$i]->cn[0],
                            "status" => ($all[$i]->nsaccountlock) ? "inactivo" : "activo",
                            "employeenumber" => (isset($all[$i]->employeenumber)) ? $all[$i]->employeenumber[0] : "N/A",
                            
                        )
                    );
            }
        }
        return view('freeipa.activos', compact('data'));
    }

    public function create()
    {
        return view('freeipa.register', [
            'addresses' => Address::all(),
            'cargo' => Cargo::all(),
            'tipo_empleado' =>TipoEmpleado::all()
        ]);
    }
    public function search()
    {
        return view('freeipa.search');
    }

    public function edit($cedula)
    {
        $ipa = Freeipa::Consultas();
        $ced = $ipa->user()->findBy('employeenumber', $cedula);
        if(isset($ced[0]->employeetype[0])){
            $tipo = $ced[0]->employeetype[0];
        }else{
            $tipo = '';
        }
        if(isset($ced[0]->carlicense[0])){
            $fecha = $ced[0]->carlicense[0];
            $fecha = str_replace('/', '-', $fecha);
            $fecha = date('d-m-Y', strtotime($fecha));
        }else{
            $fecha = '';
        }

        if(isset($ced[0]->telephonenumber[0])){
            $ext = $ced[0]->telephonenumber[0];
        }else{
            $ext = '';
        }
        $tipo_empleado = TipoEmpleado::all();
        $addresses = Address::all();

        $cedula = $ced[0]->employeenumber[0];

        $cargo = Cargo::all();

        

        

        return view('freeipa.edit', compact('ced','addresses','tipo_empleado','tipo','fecha','ext','cedula','cargo'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    function TempUpdate($cedula){

        $ipa = Freeipa::Consultas();
        $ced = $ipa->user()->findBy('employeenumber', $cedula);

        $modify_user_data = array(
            'givenname'       => 'TempName',
            'sn'              => 'TempLastname',
        );

        try {
            $modify_user = $ipa->user()->modify($ced[0]->uid[0], $modify_user_data);
            if ($modify_user) {

            } else {
                echo '¡Error!';
                return response()->json(['message'=>'No se pudo crear el usuario'], 500);
            }
        } catch (\Exception $e) {
            return response()->json(['message'=>'Debe Realizar un cambio en el formulario'], 500);
        }

    }
    public function update(Request $request, $cedula)
    {
        $ipa = Freeipa::Consultas();
        $ced = $ipa->user()->findBy('employeenumber', $cedula);

        $cedula = $request->cedule;
        $nombres = ucwords($request->name);
        $apellidos = ucwords($request->lastname);
        $cargo = $request->load;
        $tipo = $request->type;
        $fecha = $request->date;
        $ext = $request->phone;
        $validar = $ipa->user()->findBy('employeenumber', $request->cedule);
        if ($validar) {

            if ($ced[0]->uidnumber[0] != $validar[0]->uidnumber[0]) {
                
                return response()->json(['message'=>'¡La cedula ya se encuentra registrada!'], 500);
            }
        }
            $new_user_data = array(
            'uid'  => $ced[0]->uid[0],
            );

           $this->TempUpdate($cedula);

            $modify_user_data = array(
                'employeenumber'  => $cedula,
                'givenname'       => $nombres,
                'sn'              => $apellidos,
                'title'           => $cargo,
                'employeetype'    => $tipo,
                'carlicense'      => $fecha,
                'telephonenumber' => $ext,
            );
            try {
                

                $modify_user = $ipa->user()->modify($new_user_data['uid'], $modify_user_data);
                if ($modify_user) {

                    $bitacora = new Bitacora;
                    $bitacora->user_end = $ced[0]->uid[0];
                    $bitacora->user_id = $_SESSION['id'];
                    $bitacora->action_id = 2;
                    $bitacora->save();  

                    return response()->json(['message' => 'El empleado <b>'.$nombres.' '.$apellidos.'</b> ha sido modificado correctamente']);

                } else {
                    echo '¡Error!';
                    return response()->json(['message'=>'No se pudo crear el usuario'], 500);
                }
            } catch (\Exception $e) {
                return response()->json(['message'=>'Debe Realizar un cambio en el formulario'], 500);
            }
    }

    public function email(Request $request, $cedula)
    {
        $ipa = Freeipa::Consultas();
        $ced = $ipa->user()->findBy('employeenumber', $cedula);

        
        
            $new_user_data = array(
            'uid'  => $ced[0]->uid[0],
            );
            $modify_user_data = array(
                'mail'  => 'jubilados@vicepresidencia.gob.ve',
            );
                $modify_user = $ipa->user()->modify($new_user_data['uid'], $modify_user_data);
                if ($modify_user) {
                    
                    $bitacora = new Bitacora;
                    $bitacora->user_end = $ced[0]->uid[0];
                    $bitacora->user_id = $_SESSION['id'];
                    $bitacora->action_id = 6;
                    $bitacora->save();
                    

                    return response()->json(['message' => 'El correo del jubilado ha sido modificado correctamente']);

                } else {
                    echo '¡Error!';
                    return response()->json(['message'=>'No se pudo modificar el usuario'], 500);
                }


    }

    public function disable(Request $request, $cedula)
    {
        $ipa = Freeipa::Consultas();
        $ced = $ipa->user()->findBy('employeenumber', $cedula);

        
        

        if ($ced[0]->nsaccountlock == 'false') {
            $disable = 'false';
        }else{
            $disable = 'true';
        }
        
            $new_user_data = array(
            'uid'  => $ced[0]->uid[0],
            );
            $modify_user_data = array(
                'nsaccountlock'  => $disable,
            );
                if ($disable == 'false') {
                    $sms = 'Activado';
                }else{
                    $sms = 'Desactivado';
                }
            
                $modify_user = $ipa->user()->modify($new_user_data['uid'], $modify_user_data);
                if ($modify_user) {
                    
                    if ($ced[0]->nsaccountlock == 'false') {
                        $bitacora = new Bitacora;
                        $bitacora->user_end = $ced[0]->uid[0];
                        $bitacora->user_id = $_SESSION['id'];
                        $bitacora->action_id = 5;
                        $bitacora->save();
                    }else{
                        $bitacora = new Bitacora;
                        $bitacora->user_end = $ced[0]->uid[0];
                        $bitacora->user_id = $_SESSION['id'];
                        $bitacora->action_id = 4;
                        $bitacora->save();
                    }

                    return response()->json(['message' => 'El empleado  ha sido '.$sms.' correctamente']);

                } else {
                    echo '¡Error!';
                    return response()->json(['message'=>'No se pudo modificar el usuario'], 500);
                }
            
    }

    public function store(Request $request)
    {
        $cedula = $request->cedule;
        $nombres = $request->name;
        $apellidos = $request->lastname;
        $cargo = $request->load;
        $tipo = $request->type;
        $fecha = $request->date;
        $direccion = $request->address;
        $ext = $request->phone;

        $ipa = Freeipa::Consultas();
        $ced = $ipa->user()->findBy('employeenumber', $cedula);
        $cedSist = Person::where('cedula', $cedula)->first();

        if ($ced or $cedSist) {
            return response()->json(['message'=>'¡La cedula ya se encuentra registrada!'], 500);
        }
        else{
            $personal = new Person;
            $personal->cedula = $cedula;
            $personal->nombre = $nombres;
            $personal->apellido = $apellidos;
            $personal->cargo = $cargo;
            $personal->tipoempleado = $tipo;
            $personal->address = $direccion;
            $personal->fecha = $fecha;
            $personal->ext = $ext;
            $personal->save();

            $bitacora = new Bitacora;
            $bitacora->user_end = $request->name;
            $bitacora->user_id = $_SESSION['id'];
            $bitacora->action_id = 1;
            $bitacora->save();

            return response()->json(['message' => 'El empleado <b>'.$nombres.' '.$apellidos.'</b> ha sido registrado correctamente. ']);
        }
    }

    
    public function searchUser(Request $request) {
        $ipa = Freeipa::Consultas();
        $cedula = $request->cedula;
        $r = $ipa->user()->findBy('employeenumber', $cedula);
        if ($r) {

            foreach ($r as $key) {
                foreach($key->memberof_group as $k){
                    $direc = Address::where('conversion', $k)->get();
                    if($direc != '[]') {
                        $dir = $direc[0]->name;
                    }else {
                        $dir = 'Debe asignar una dirección de adscripción';
                    }

                    if(isset($key->carlicense)){
                        $fecha = $key->carlicense[0];
                        $fecha = str_replace('/', '-', $fecha);
                        $fecha = date('d-m-Y', strtotime($fecha));
                    }else{
                        $fecha = 'Debe Asignar la Fecha de ingreso';
                    }

                    if($key->nsaccountlock == true){
                        $status = 'Inactivo';
                    }else{
                        $status = 'Activo';
                    }

                    if (isset($key->telephonenumber)) {
                        $telefono = $key->telephonenumber[0];
                    }else {
                        $telefono = 'Debe Asignar la EXT';
                    }

                    if (isset($key->title)) {
                        $cargo = $key->title[0];
                    }else {
                        $cargo = 'Debe Asignar el cargo';
                    }


                }
                echo '
                    <div class="card offset-3 text-center" style="width: 50%;">
                      <div class="card-header">
                        <b>Trabajador/Personal</b>
                      </div>
                      <ul class="list-group list-group-flush">
                        <li class="list-group-item"><b>Cédula de identidad:</b> '.$key->employeenumber[0].'</li>
                        <li class="list-group-item"><b>Nombres y Apellidos:</b> '.$key->cn[0].'</li>
                        <li class="list-group-item"><b>Departamento / Dirección:</b> '.$dir.'</li>
                        <li class="list-group-item"><b>Cargo:</b> '.$cargo.'</li>
                        <li class="list-group-item"><b>Usuario:</b> '.$key->uid[0].'</li>
                        <li class="list-group-item"><b>Correo:</b> '.$key->mail[0].'</li>
                        <li class="list-group-item"><b>Ext o Teléfono:</b> '.$telefono.'</li>
                        <li class="list-group-item"><b>Fecha de ingreso:</b> '.$fecha.'</li>
                        <li class="list-group-item"><b>Estatus:</b> '.$status.'</li>
                        <li class="list-group-item">
                            <a href="../freeipa/edit/'.$key->employeenumber[0].'" title="Editar Usuario" class="text-right text-white btn btn-primary form-control-sm"><i class="mdi mdi-table-edit"></i>
                            </a>
                            <a href="#" onclick="Status('.$key->employeenumber[0].')"title="Cambiar Estatus" class="text-right text-white btn btn-primary form-control-sm"><i class="mdi mdi-shuffle-disabled"></i>
                            </a>
                    ';
                    if (substr(strtoupper($key->title[0]), 0, -1) == 'JUBILAD'  and $key->mail[0] != 'jubilados@vicepresidencia.gob.ve' ) {
                        echo '<a href="#" onclick="Email('.$key->employeenumber[0].')"title="Actualizar correo de personal jubilado" class="text-right text-white btn btn-primary form-control-sm"><i class="mdi mdi-email"></i>
                            </a>';
                    }else{
                        
                    }


                    echo '
                        </li>
                        
                    </div>
                    ';
            }
        }
        else {
            echo'<div class="row mt-2 pb-3 text-danger">
                    <div class="col-md-12 text-center">
                        <h3><i class="mdi mdi-account-remove"></i> No se encontraron registros</h3>
                    </div>
                </div>';
        }
    }
}
