<?php

namespace App\Http\Controllers;
use Illuminate\Http\Request;
use App\User;
use App\Address;
use App\Bitacora;
use App\Roles;
use App\Http\Controllers\freeipa\Freeipa;

class UserController extends Controller
{
    public function index()
    {
        return view('pages.admin.create');
    }

    public function bitacora()
    {
        $bit = Bitacora::all();
        return view('pages.admin.bitacora', compact('bit'));
    }

    public function buscar(Request $request) {
        $ipa = Freeipa::Consultas();
        $cedula = $request->cedula;
        $r = $ipa->user()->findBy('employeenumber', $cedula);

        if ($r) {
            $roles = Roles::all();            
            foreach ($r as $key) {
                if (isset($key->telephonenumber)) {
                    $telefono = $key->telephonenumber[0];
                }else {
                    $telefono = 'Debe Asignar la EXT';
                }
                if(isset($key->carlicense)){
                    $fecha = $key->carlicense[0];
                    $fecha = str_replace('/', '-', $fecha);
                    $fecha = date('d-m-Y', strtotime($fecha));
                }else{
                    $fecha = 'Debe Asignar la Fecha de ingreso';
                }
                if($key->nsaccountlock == true){
                    $status = 'Inactivo';
                }else{
                    $status = 'Activo';
                }
                foreach($key->memberof_group as $k){
                    $direc = Address::where('conversion', $k)->get();
                    if($direc != '[]') {
                        $dir = $direc;
                    }
                }
                echo '
                    <div class="card offset-3 text-center" style="width: 50%;">
                      <div class="card-header">
                        <b>Trabajador/Personal</b>
                      </div>
                      <ul class="list-group list-group-flush">
                        <li class="list-group-item"><b>Cédula de identidad:</b> '.$key->employeenumber[0].'</li>
                        <li class="list-group-item"><b>Nombres y Apellidos:</b> '.$key->cn[0].'</li>
                        <li class="list-group-item"><b>Departamento / Dirección:</b> '.$dir[0]->name.'</li>
                        <li class="list-group-item"><b>Usuario:</b> '.$key->uid[0].'</li>
                        <li class="list-group-item"><b>Ext:</b> '.$telefono.'</li>
                        <li class="list-group-item"><b>Fecha de ingreso:</b> '.$fecha.'</li>
                        <li class="list-group-item"><b>Estatus:</b> '.$status.'</li>
                        <li class="list-group-item"><b>Seleccione el tipo de usuario:</b>
                            <select name="rol" id="rol" class="form-control-sm form-comtrol" required>
                                <option value="0" disabled selected>Seleccione...</option>
                                        ';
                                foreach ($roles as $rol) {
                                    echo '
                                    <option value="'.$rol->id.'">'.$rol->option.'</option>
                                    ';
                                }
                                echo '                              
                            </select>
                        </li>
                        <li class="list-group-item"><a data-toggle="modal" data-target="#pass" id="addUsuario" name="addUsuario" class="btn btn-primary text-light">Enviar</a>
                        </li>
                        
                    </div>
                    <input type="hidden" class="form-control" name="nombre" value="'.$key->uid[0].'">
                    <input type="hidden" class="form-control" name="direc" value="'.$dir[0]->id.'">
                <script>
                   $("#adUs").validate({
                        rules: {
                           rol: {required:true},
                           pass: {required:true},
                        },
                        messages: {
                           rol : "¡Dede seleccionar un tipo de usuario!",
                           pass : "¡Dede agregar su contraseña!",
                        },
                    });
                </script>                       
                ';
            }
        }
        else {
            echo'<div class="row mt-2 pb-3 text-danger">
                    <div class="col-md-12 text-center">
                        <h3><i class="mdi mdi-account-remove"></i> No se encontraron registros</h3>
                    </div>
                </div>';
        }
    }

    public function store(Request $request)
    {
        $u = User::where('user', $request->nombre)->get();
        $tipo = Roles::find($request->rol);

        if($request->pass == $_SESSION['pass'])
        {
            if($u == '[]') {
                $user = new User;
                $user->user = $request->nombre;
                $user->rol_id = $request->rol;

                $bitacora = new Bitacora;
                $bitacora->user_end = $request->nombre;
                $bitacora->user_id = $_SESSION['id'];
                $bitacora->action_id = 7;
                $bitacora->save();

                $user->save();
                return response()->json(['message'=>'¡Usuario de '.$tipo->opcion.' agregado correctamente!'], 200);
            }else{
                if ($u[0]->rol == $request->rol) {
                	return response()->json(['message'=>'¡Este usuario ya pertenece a '.$tipo->opcion.'!'], 500);

                }else{
                    $id = $u[0]->id;

                    $user = User::find($id);
                    $user->rol_id = $request->rol;

                    $user->save();
                    return response()->json(['message'=>'¡Usuario cambiado al rol '.$tipo->option.'!'], 200);
                }
             }
        }else{
        	return response()->json(['message'=>'¡Contraseña incorrecta!'], 500);
        }
    }
}
