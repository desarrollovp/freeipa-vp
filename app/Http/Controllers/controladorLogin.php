<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Http\Controllers\freeipa\Freeipa;
use Illuminate\Support\Facades\Crypt;
use App\User;
use Carbon\Carbon;

class controladorLogin extends Controller
{
	public function login(Request $request) 
	{

		$user = request('user');
		$pass  = request('pass');
		$clave = Crypt::encrypt($pass);

		$auth = Freeipa::Login($user, $clave);
		
		
		if ($auth) {

			$permiso = User::where('user', $user)->get();

			if ($permiso != "[]") {

			  	$now = new \DateTime();

				$nombre  = $auth->user()->findBy('uid', $user);
				$_SESSION['intervalo']  = 5; //tiempo de vida de la sesión en minutos
				$_SESSION['inicio'] = time();
				$_SESSION['nombre'] = $nombre[0]->cn[0];
				$_SESSION['user'] = $user;
				$_SESSION['rol'] = $permiso[0]->rol_id;
				$_SESSION['id'] = $permiso[0]->id;
				$_SESSION['pass'] = $pass;

				return redirect('/');

			}else {
				
				return back()->with('msj', '¡No tienes permiso para ingresar a este sistema!');
			}

		} else {

		 		return back()->with('errormsj', 'Usuario o contraseña incorrectos');

			}

	} 

	public function logout()
	{
			session_destroy(); 
			return redirect('/');
	}


	
}
