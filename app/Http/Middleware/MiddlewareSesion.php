<?php

namespace App\Http\Middleware;

use Closure;
use App\Person;

class MiddlewareSesion
{
    /**
     * Handle an incoming request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \Closure  $next
     * @return mixed
     */
    public function handle($request, Closure $next)
    {
        if(isset($_SESSION['nombre']))
        {
            $personal = Person::where('correo', null)->get();
            $_SESSION['notificaciones'] = $personal;

            if($_SESSION['rol'] == '2' ){
                $segundos = time();
                $tiempo_transcurrido = $segundos;
                $tiempo_maximo = $_SESSION['inicio']  + ( $_SESSION['intervalo'] * 60 ); 
                if($tiempo_transcurrido > $tiempo_maximo){
                    return redirect('/logout');
                }else{
                    $_SESSION['inicio'] = time();
                }    
            
            }

            return $next($request); 
        
        }

        else
        {
            return redirect('/login');
        }

    }
}
