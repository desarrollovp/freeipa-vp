<?php

namespace App\Http\Middleware;

use Closure;

class MiddlewareRRHH
{
    /**
     * Handle an incoming request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \Closure  $next
     * @return mixed
     */
    public function handle($request, Closure $next)
    {
        if($_SESSION['rol'] == '2' || $_SESSION['rol'] == '1')
        {
            return $next($request); 
            
        }

        else
        {
            return redirect('/');
        }
    }
}
