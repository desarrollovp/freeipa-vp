<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Bitacora extends Model
{
    protected $table = 'bitacora';

    public function action()
	{
		return $this->belongsTo('App\Action', 'action_id');
    }

    public function user()
	{
		return $this->belongsTo('App\User', 'user_id');
    }
}
