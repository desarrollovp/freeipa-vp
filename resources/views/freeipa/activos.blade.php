@extends('layouts.app')
@section('title', 'Sistema de Registro')
@section('subtitle', 'Registro del empleado para el sistema Freipa')
@section('css')
{{-- ESTILOS  --}}
@endsection
@section('content')
  <div class="col-md-12 stretch-card">
    <div class="card">
      <div class="card-header bg-gradient-dark text-center p-4">
        <h4 class="text-white">Registros de usuarios activos</h4>
      </div>
      <div class="card-body bg-light">
        <div class=" table-responsive text-center">
                <table id="tabla1" class="table table-bordered table-hover" data-order='[[ 0, "desc" ]]' data-page-length='10'>
                        <thead>
	              <tr>
	                
    				<th>User</th>
    				<th>Nombres</th>
    				<th>Cédula</th>
    				<th>Status</th>
	              </tr>
	            </thead>
	            <tbody>
						@for ($i = 0; $i < count($data); $i++)
		            		<tr>
							    <td>{{ $data[$i]['uid'] }}</td>
							    <td>{{ $data[$i]['cn'] }}</td>
							    <td>{{ $data[$i]['employeenumber'] }}</td>
							    <td>{{ $data[$i]['status'] }}</td>
							</tr>
						@endfor	            	
						
	            </tbody>
	        </table>
        </div>
      </div>
    </div>
  </div>
@endsection
@section('js')
<script>
	function dataTableExport(title, columns) {
    $('#tabla1').DataTable( {
        lengthChange: true,
        lengthMenu:[10,25,50,100],
        dom: "<'row'<'col-sm-12 col-md-2'l><'col-sm-12 col-md-4'B><'col-sm-12 col-md-6'f>>t<ip>",
        buttons: [
            {
                extend: 'excel',
                title: title,
                text: '<img src="http://localhost:8000/img/excel-ico.png" alt="" heigth />',
                titleAttr: 'Exportar CSV',
                exportOptions: {
                    columns: columns
                }

            }
        ],
    } );
}
</script>
<script>

    var title = 'Usuarios';
    var columns = [0,1,2,3];

    $(".file").fileinput({
        // theme: 'gly',
        // uploadUrl: '#',
        showCaption: false,
        showRemove: false,
        showUpload: false,
        showBrowse: false,
        browseOnZoneClick: true,
    });

    dataTableExport(title,columns);

</script>

@endsection