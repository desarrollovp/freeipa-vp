@extends('layouts.app')
@section('title', 'Sistema de Registro')
@section('subtitle', 'Registro del empleado para el sistema Freipa')
@section('css')
{{-- ESTILOS  --}}
@endsection
@section('content')
  <div class="col-md-12 stretch-card">
    <div class="card">
      <div class="card-header bg-gradient-dark text-center p-4">
        <h4 class="text-white">Editar Usuario</h4>
      </div>
      <div class="card-body bg-light">
        <div class="container-fluid mt-4">
          <form method="POST" id="formEdit" name="formEdit">
          <!-- <form method="POST" action="{{ url('freeipa/update', $cedula) }}"> -->
            <input type="hidden" name="_method" value="PUT">
            <input type="hidden" name="id" value="{{ $ced[0]->uidnumber[0]}}">
            {{ csrf_field() }}
            <div class="row justify-content-center">
              <div class="col-md-6">
                <div class="form-group row ">
                  <label class="col-sm-4 col-form-label "><i class="ml-4 h5 text-danger  mdi mdi-alert-circle-outline " title="Campo reqiero"></i> Cedula:</label>
                  <div class="col-sm-8">
                    <input onkeypress="return soloNum(event)" type="text" class="form-control shadow rounded" name="cedule" id="cedule" value="{{ $ced[0]->employeenumber[0]}}" />
                  </div>
                </div>
              </div>
              <div class="col-md-6">
                <div class="form-group row ">
                  <label class="col-sm-4 col-form-label"><i class="ml-4 h5 text-danger  mdi mdi-alert-circle-outline " title="Campo reqiero"></i> Nombres:</label>
                  <div class="col-sm-8">
                    <input type="text" class="form-control shadow rounded" value="{{ $ced[0]->givenname[0]}}" name="name" id="name" />
                  </div>
                </div>
              </div>
            </div>
            <div class="row justify-content-center">
              <div class="col-md-6">
                <div class="form-group row ">
                  <label class="col-sm-4 col-form-label"><i class="ml-4 h5 text-danger  mdi mdi-alert-circle-outline " title="Campo reqiero"></i> Apellidos:</label>
                  <div class="col-sm-8">
                    <input type="text" value="{{ $ced[0]->sn[0]}}" class="form-control shadow rounded" name="lastname" id="lastname" />
                  </div>
                </div>
              </div>
              <div class="col-md-6">
                <div class="form-group row ">
                  <label class="col-sm-4 col-form-label"><i class="ml-4 h5 text-danger  mdi mdi-alert-circle-outline " title="Campo reqiero"></i> Cargo:</label>
                  <div class="col-sm-8">
                    <div class="shadow rounded">
                      <select class="form-control  select" name="load" id="load">
                        <option value="{{ $ced[0]->title[0]}}">{{ $ced[0]->title[0]}}</option>
                        @foreach($cargo as $car)
                          <option value="{{$car->opcion}}">{{$car->opcion}}</option>
                        @endforeach
                      </select>
                    </div>
                  </div>
                </div>
              </div>
            </div>
            <div class="row justify-content-center">
              <div class="col-md-6">
                <div class="form-group row ">
                  <label class="col-sm-4 col-form-label"><i class="ml-4 h5 text-danger  mdi mdi-alert-circle-outline " title="Campo reqiero"></i> Tipo de empleado:</label>
                  <div class="col-sm-8">
                    <div class="shadow rounded">
                      <select class="form-control select" name="type" id="type">
                          <option  disabled selected>Debe asignar el tipo de empleado...</option>
                          @foreach($tipo_empleado as $tip)
                            <option value="{{$tip->opcion}}" @if( $tipo == $tip->opcion ) selected @endif >{{$tip->opcion}}</option>
                          @endforeach
                        </select>
                    </div>
                  </div>
                </div>

              </div>
              <div class="col-md-6">
                <div class="form-group row ">
                  <label class="col-sm-4 col-form-label"><i class="ml-4 h5 text-danger  mdi mdi-alert-circle-outline " title="Campo reqiero"></i> Fecha de ingreso:</label>
                  <div class="col-sm-8">
                    <input type="text" class="form-control shadow rounded calendar" name="date" id="date" value="{{ $fecha }}" placeholder="Debe asignar una fecha de ingreso" onkeypress="return disable(event)" />
                  </div>
                </div>
              </div>

            </div>
            <div class="row justify-content-center">
              
              <div class="col-md-6">
                <div class="form-group row ">
                  <label class="col-sm-4 col-form-label"><span class="ml-5"> Extensión:</span></label>
                  <div class="col-sm-8">
                    <input onkeypress="return soloNum(event)" type="text" class="form-control shadow rounded" name="phone" placeholder="Debe asignar la ext del empleado" value="{{ $ext }}" id="phone" />
                  </div>
                </div>
              </div>

              <div class="col-md-6" readonly>
                <div class="form-group row ">
                  <label class="col-sm-4 col-form-label"><span class="ml-5"> Correo:</span></label>
                  <div class="col-sm-8">
                    <input readonly type="text" class="form-control shadow rounded"  placeholder="Debe asignar correo" value="{{$ced[0]->mail[0] }}" />
                  </div>
                </div>
              </div>
            </div>

           
            <div class="text-center">
              <p>Los campos marcados (<i class="h5 text-danger  mdi mdi-alert-circle-outline " title="Campo reqiero"></i>) son requeridos</p>
              <button type="submit" class="btn btn-primary mr-2">Modificar</button>
              <a href="{{ url()->previous() }}" class="btn btn-secondary">Cancel</a>
            </div>
          </form>
        </div>
      </div>
    </div>
  </div>
@endsection
@section('js')
{{-- SCRIPTS  --}}
  <script src="{{ asset('js/validate.js') }}"></script>
  <script type="text/javascript">
  $(document).ready(function(){
    $('#formEdit').validate({
      rules: {
        cedule: {required:true, minlength:7},
        name: {required:true, minlength:5},
        lastname: {required:true, minlength:5},
        load: {required:true},
        type: {required:true},
        address: {required:true},
        date: {required:true},

      },
      messages: {
        cedule : {required: "¡Ingrese el número de cédula!", minlength: "¡Ingrese un número de cédula válido!"},
        name : {required:"¡Ingrese al menos un nombre!", minlength: "¡El nombre dete tener al menos 5 caracteres!"},
        lastname : {required:"¡Ingrese al menos un apellido!", minlength: "¡El apellido al menos 5 caracteres!"},
        load : "¡Indique el cargo!",
        type: "¡Indique el tipo de empleado!",
        address: "¡Seleccione una dirección de adscripción!",
        date: "¡Seleccione la fecha de ingreso!",

      },
      submitHandler: function(form){
        var data = $('#formEdit').serialize();
        $.ajax({
          url: '{{ url('freeipa/update', $cedula) }}',
          type: 'POST',
          data: data,
          success: function(data){
            Swal.fire({
                title:'Exitoso!',
                html: data.message,
                type: 'success',
                confirmButtonText: 'Aceptar'
            }).then((result) => {
            $('#formEdit')[0].reset();
            location.reload();
            })
          },
          error: function(data) {
            toastr.error(data.responseJSON.message, 'Error!')
          },
        })
      }
    });
  });

  // In your Javascript (external .js resource or <script> tag)
  $(document).ready(function() {
      $('.select').select2({
        theme: "bootstrap4",
      });
  });
  </script>
@endsection