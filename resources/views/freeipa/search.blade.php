@extends('layouts.app')
@section('title', 'Sistema de Registro')
@section('subtitle', 'Registro del empleado para el sistema Freipa')
@section('css')
{{-- ESTILOS  --}}
@endsection
@section('content')
  <div class="col-md-12 stretch-card">
    <div class="card">
      <div class="card-header bg-gradient-dark text-center p-4">
        <h4 class="text-white">Datos del empleado</h4>
      </div>
      <div class="card-body bg-light">
        <div class="container-fluid mt-4">
          <form method="POST" role="form" id="formUser" name="formUser" class="form-horizontal">
              {{ csrf_field() }}
                <div class="col-md-4 offset-4 form-estilo">
                    <input onkeypress="return soloNum(event)" autofocus maxlength="8" type="text" id="cedula" @if(isset($cedula)) value="{{ $cedula }}" @endif name="cedula" class="form-control mb-3 text-center" placeholder="Ingrese la cédula">
                </div>

                <div class="col-md-12 mb-4">
                  <center><a href="#" class="btn btn-primary mr-2" onclick="searchUser()">Buscar</a></center>
                </div>
              </form>
              <form  method="POST" role="form" id="addAsistencia" name="addAsistencia" class="form-horizontal">
                {{ csrf_field() }}
                  <div id="resultados">
                  </div>
              </form>
        </div>
      </div>
    </div>
  </div>
@endsection
@section('js')
<script src="{{ asset('js/validate.js') }}"></script>
<script>
  $(document).ready(function() {
    $('.dataTable').DataTable({
      "order": [[ 0, 'desc' ]]
    });

    $('#cedule').val($cedula);
});

function searchUser(){
    var data  = $('#formUser').serialize();
  $.ajax({
    url: '{{ url('freeipa/searchUser') }}',
    type: 'POST',
    data: data,
    success: function(data){
      $('#resultados').html(data);
    }
  }) 
}

function Status(id) {
  $.ajax({
  url: '{{ url("freeipa/disable")}}/'+id,
  type: 'GET',
  success: function(data){
    Swal.fire({
        title:'Exitoso!',
        html: data.message,
        type: 'success',
        confirmButtonText: 'Aceptar'
    }).then((result) => {
      searchUser()
    })
  },
  error: function(data) {
    toastr.error(data.responseJSON.message, 'Error!')
  },
})
}

function Email(cedula) {
  $.ajax({
  url: '{{ url("freeipa/email")}}/'+cedula,
  type: 'GET',
  success: function(data){
    Swal.fire({
        title:'Exitoso!',
        html: data.message,
        type: 'success',
        confirmButtonText: 'Aceptar'
    }).then((result) => {
      searchUser()
    })
  },
  error: function(data) {
    toastr.error(data.responseJSON.message, 'Error!')
  },
})
}
</script>


@endsection