@extends('layouts.app')
@section('title', 'Sistema de Registro')
@section('subtitle', 'Registro del empleado para el sistema Freipa')
@section('css')
{{-- ESTILOS  --}}
@endsection
@section('content')
  <div class="col-md-12 stretch-card">
    <div class="card">
      <div class="card-header bg-gradient-dark text-center p-4">
        <h4 class="text-white">Registros de usuarios sin correo</h4>
      </div>
      <div class="card-body bg-light">
        <div class="container-fluid mt-4">
			<table class="dataTable table-striped table-bordered table-hover text-center w-100">
	            <thead>
	              <tr>
	                <th>Cédula</th>
    				<th>Nombres</th>
	                <th>Apellidos</th>
    				<th>Correo</th>
	              </tr>
	            </thead>
	            <tbody>
	             	@foreach($personal as $data)
						<tr>
							<td><a href="#" hidden>{{$data->id}}</a>{{$data->cedula}}</td>
							<td><a href="correo/{{$data->id}}">{{strtoupper($data->nombre)}}</a></td>
							<td>{{$data->apellido}}</td>
							@if($data->correo)
							    <td>{{ $data->correo }}</td>
							@else
							    <td>Usuario sin correo</td>
							@endif
						</tr>
					@endforeach
	            </tbody>
	        </table>
        </div>
      </div>
    </div>
  </div>
@endsection
@section('js')
<script>
	$('.dataTable').DataTable({
      "order": [[ 0, 'desc' ]]
    });
</script>

@endsection