@extends('layouts.app')
@section('title', 'Sistema de Registro')
@section('subtitle', 'Registro del empleado para el sistema Freipa')
@section('css')
{{-- ESTILOS  --}}
@endsection
@section('content')
<div class="col-md-12 stretch-card">
    <div class="card">
      <div class="card-header bg-gradient-dark text-center p-4">
        <h4 class="text-white">Datos del usuario <b>"{{ strtoupper($correo->nombre) }} {{ strtoupper($correo->apellido) }}"</b></h4>
      </div>
    <div class="card-body bg-light">
        <div class="container-fluid mt-4">
		    <div class="col-md-6 offset-3 grid-margin stretch-card">
		              <div class="card">
		                <div class="card-body">
		                  <h4 class="card-title">Asignar Correo</h4>
		                  <form class="forms-sample"  method="POST" role="form" id="asignarCorreo" name="asignarCorreo">
		                  	<input type="hidden" name="_method" value="PUT">
								        {{ csrf_field() }}
		                    <div class="form-group">
		                      <label for="nombre">Nombres y Apellidos</label>
		                      <input type="text" class="form-control" name="nombre" id="nombre" placeholder="Nombre y Apellido" value="{{ strtoupper($correo->nombre) }} {{ strtoupper($correo->apellido) }}" readonly>
		                    </div>
		                    <div class="form-group">
		                      <label for="cedula">Cédula</label>
		                      <input type="text" class="form-control" name="cedula" id="cedula" placeholder="Cédula" value="{{ $correo->cedula }}" readonly>
		                    </div>
                        <div class="form-group">
                          <label for="cedula">Dirección</label>
                          <input type="text" class="form-control" name="direccion" id="direccion" placeholder="Cédula" value="{{ $correo->address }}" readonly>
                        </div>

		                    <div class="form-group">
		                     	<label for="correo">Correo <i class="h5 text-danger  mdi mdi-alert-circle-outline " title="Campo reqiero"></i></label>
			                    <div class="input-group">
			                      <div class="input-group-prepend">
			                        <span class="input-group-text">@</span>
			                      </div>
			                      <input type="text" class="form-control" name="correo" id="correo" placeholder="Aisgnar correo">
			                      <div class="input-group-prepend">
			                        <span class="input-group-text text-dark">@vicepresidencia.gob.ve</span>
			                      </div>
			                    </div>
			                </div>
			                <div class="row">
							   <div class="col-md-12 text-danger">
								   <div class="js-errors">
								   </div>
							   </div>
						   </div>
			                 <p>Los campos marcados (<i class="h5 text-danger  mdi mdi-alert-circle-outline " title="Campo reqiero"></i>) son requeridos</p>
		                    <button type="submit" class="btn btn-primary mr-2">Enviar</button>
		                    <a href="{{ url()->previous() }}" class="btn btn-secondary">Cancel</a>
		                  </form>
		                </div>
		              </div>
				</div>
			</div>
    	</div>
  	</div>
</div>
@endsection
@section('js')
{{-- SCRIPTS  --}}
  <script src="{{ asset('js/validate.js') }}"></script>
  <script type="text/javascript">
  $(document).ready(function(){
    $('#asignarCorreo').validate({
      rules: {
        correo: {required:true, minlength:5},
      },
      messages: {
        correo : {required: "¡Ingrese el correo a asignar!"},
      },
      highlight: function(element, errorClass, validClass) {
            $(element).addClass(errorClass).removeClass(validClass);
            // Keeps the default behaviour, adding error class to element but seems to break the default radio group behaviour but the below fixes that
            $(element).closest('ul').addClass(errorClass);
            // add error class to ul element for checkbox groups and radio inputs
        },
        unhighlight: function(element, errorClass, validClass) {
            $(element).removeClass(errorClass).addClass(validClass);
            // keeps the default behaviour removing error class from elements
            $(element).closest('ul').removeClass(errorClass);
            // remove error class from ul element for checkbox groups and radio inputs
        },
        // FIX END

        errorLabelContainer: ".js-errors",
        errorElement: "h6",

      submitHandler: function(form){
        var data = $('#asignarCorreo').serialize();
        $.ajax({
          url: '{{ url('datacenter/update', $correo->id) }}',
          type: 'POST',
          data: data,
          success: function(data){
            Swal.fire({
                title:'Exitoso!',
                html: data.message,
                type: 'success',
                confirmButtonText: 'Aceptar'

            }).then((result) => {
            //$('#form')[0].reset();
             window.location.href = "{{ route('datacenter.show') }}";
            })
          },
          error: function(data) {
            toastr.error(data.responseJSON.message, 'Error!')
          },
        })
      }

    });
  });
  </script>
@endsection