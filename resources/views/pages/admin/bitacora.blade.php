@extends('layouts.app')
@section('title', 'Sistema de Registro')
@section('subtitle', 'Registro del empleado para el sistema Freipa')
@section('css')
{{-- ESTILOS  --}}
@endsection
@section('content')
  <div class="col-md-12 stretch-card">
    <div class="card">
      <div class="card-header bg-gradient-dark text-center p-4">
        <h4 class="text-white">Bitácora del sistema</h4>
      </div>
      <div class="card-body bg-light">
        <div class="container-fluid mt-4">
			<table class="dataTable table-striped table-bordered table-hover text-center w-100">
	            <thead>
	              <tr>
	                <th>ID</th>
    				<th>Usuario</th>
	                <th>Acción</th>
    				<th>Usuario Final</th>
	              </tr>
	            </thead>
	            <tbody>
	             	@foreach($bit as $data)
						<tr>
							<td><a href="#" hidden>{{$data->id}}</a>{{$data->id}}</td>
							<td>{{strtoupper($data->user->user)}}</td>
							<td>{{$data->action->option}}</td>
							<td>{{$data->user_end}}</td>
						</tr>
					@endforeach
	            </tbody>
	        </table>
        </div>
      </div>
    </div>
  </div>
@endsection
@section('js')
<script>
	$('.dataTable').DataTable({
      "order": [[ 0, 'desc' ]]
    });
</script>

@endsection