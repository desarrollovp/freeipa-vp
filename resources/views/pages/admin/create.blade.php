@extends('layouts.app')
@section('title', 'Sistema de Asistencias Vicepresidencia')
@section('subtitle', 'Sistema para el registro y control de asistencias de los usuarios ')
@section('css')
{{-- ESTILOS  --}}
@endsection
@section('content')
  <div class="col-md-12 stretch-card">
    <div class="card">
      
      <div class="card-body">
        <p class="card-title text-center">Agregar Usuario</p>
        <div class="row">
          <div class="col-md-12 desvanecer text-center">
            @if(session()->has('msj'))
              <div class="col-md-12  alert alert-success" >
                {{session('msj')}}
              </div>
            @endif
            @if(session()->has('errormsj'))
              <div class="col-md-12  alert alert-danger" >
                {{session('errormsj')}}
              </div>
              @endif
          </div>
        </div>
        <hr>
        <div class="table-responsive">
            <div class="col-md-12">
              <form method="POST" role="form" id="formUs" name="formUs" class="form-horizontal">
				{{ csrf_field() }}
					<div class="col-md-4 offset-4 form-estilo">
							<input onkeypress="return soloNum(event)" autofocus maxlength="8" type="text" id="cedula"  name="cedula" class="text-center form-control mb-3" placeholder="Ingrese la cédula">
					</div>

					<div class="col-md-12 mb-4">
						<center><a href="#" id="searchUs" class="btn btn-primary">Buscar</a></center>
					</div>
				</form>
        	</div>

        	<form method="POST" role="form" id="adUs" name="adUs" class="form-horizontal">
				{{ csrf_field() }}
				@include('layouts.modalPass')
					<div class="col-md-12" id="resultados">
					</div>
			</form>
       		
        </div>
      </div>
    </div>
  </div>
@endsection
@section('js')
<script src="{{ asset('js/validate.js') }}"></script>
<script>
	$(document).ready(function() {
    $('#searchUs').click(function() {
      var data  = $('#formUs').serialize();
      
      $.ajax({
        url: '{{ route('user.buscar') }}',
        type: 'POST',
        data: data,
        success: function(data){
          $('#resultados').html(data);
        }
      })    
    })

    $('#addUser').click(function() {
      var data  = $('#adUs').serialize();
      
      $.ajax({
        url: '{{ route('user.store') }}',
        type: 'POST',
        data: data,
        success: function(data){
            Swal.fire({
                title:'Exitoso!',
                html: data.message,
                type: 'success',
                confirmButtonText: 'Aceptar'

            }).then((result) => {
            //$('#form')[0].reset();
             window.location.href = "{{ route('user.index') }}";
            })
          },
          error: function(data) {
            toastr.error(data.responseJSON.message, 'Error!')
          },      })    
    })
});
</script>
@endsection