<div id="pass" class="modal fade" role="dialog">
  <div class="modal-dialog">
    <!-- Modal content-->
    <div class="modal-content">
      <div class="modal-header">
        <h4 class="modal-title text-center">Ingrese su contraseña para validar la operación.</h4>
      </div>
      <div class="modal-body">
        <div class="row">
          <div class="col-md-6 offset-3 form-estilo">
            <input type="password" name="pass" id="pass" class="form-control">
          </div>
        </div>
      </div>
      <div class="modal-footer">
        <center>
          <button id="addUser" class="btn btn-info" type="button" >Agregar</button>
          <button type="button" class="btn btn-danger" data-dismiss="modal">Cancelar</button>
        </center>
      </div>
    </div>
  </div>
</div>