<nav class="sidebar sidebar-offcanvas" id="sidebar">
  <ul class="nav list-group-flush">
    <li class="nav-item">
      <a class="nav-link" href="{{route('home')}}">
        <i class="mdi mdi-home menu-icon"></i>
        <span class="menu-title">Inicio</span>
      </a>
    </li>
    @if($_SESSION['rol'] == 2 || $_SESSION['rol'] == 1)
    <li class="nav-item">
      <a class="nav-link" data-toggle="collapse" href="#ui-basic" aria-expanded="false" aria-controls="ui-basic">
        <i class="mdi mdi-circle-outline menu-icon"></i>
        <span class="menu-title">Freeipa</span>
        <i class="menu-arrow"></i>
      </a>
      <div class="collapse" id="ui-basic">
        <ul class="nav flex-column sub-menu">
          <li class="nav-item"> <a class="nav-link" href="{{route('freeipa.create')}}">Registrar</a></li>
          <!-- <li class="nav-item"> <a class="nav-link" href="{{route('activos')}}">Activos</a></li> -->
          <li class="nav-item"> <a class="nav-link" href="{{route('freeipa.search')}}">Buscar</a></li>
        </ul>
      </div>
    </li>
    @endif
    @if($_SESSION['rol'] == 3 || $_SESSION['rol'] == 1)
      <li class="nav-item">
        <a class="nav-link" data-toggle="collapse" href="#ui-basic1" aria-expanded="false" aria-controls="ui-basic1">
          <i class="mdi mdi-format-align-center menu-icon"></i>
          <span class="menu-title">Centro de datos</span>
          <i class="menu-arrow"></i>
        </a>
        <div class="collapse" id="ui-basic1">
          <ul class="nav flex-column sub-menu">
            <li class="nav-item"> <a class="nav-link" href="{{route('datacenter.show')}}">Registros</a></li>
          </ul>
        </div>
      </li>
    @endif
  </ul>
</nav>