<footer class="footer">
  <div class="d-sm-flex justify-content-center justify-content-sm-between">
    <span class="text-muted text-center text-sm-left d-block d-sm-inline-block">Dirección General de Tecnología de la Información - Copyright © 2019 Todos los derechos reservados.</span>
    <span class="float-none float-sm-right d-block mt-1 mt-sm-0 text-center"><a href="http://vicepresidencia.gob.ve/" target="_blank">Vicepresidencia de la República Bolivariana de Venezuela</a></span>
  </div>
</footer>