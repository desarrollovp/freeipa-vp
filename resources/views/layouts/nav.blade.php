    <nav class="navbar col-lg-12 col-12 p-0 fixed-top d-flex flex-row bg-gradient-light">
      <div class="navbar-brand-wrapper d-flex justify-content-center bg-gradient-dark">
        <div class="navbar-brand-inner-wrapper d-flex justify-content-between align-items-center w-100">  
          <a class="navbar-brand brand-logo text-light" href="{{route('home')}}">FreeipaVP</a>
          <button class="navbar-toggler navbar-toggler align-self-center text-white" type="button" data-toggle="minimize">
            <span class="mdi mdi-sort-variant"></span>
          </button>
        </div>  
      </div>
      <div class="navbar-menu-wrapper d-flex align-items-center justify-content-end">
        <ul class="navbar-nav navbar-nav-right">
          
          @if($_SESSION['rol'] == 3 || $_SESSION['rol'] == 1)
          <li class="nav-item dropdown mr-4">
            <a class="nav-link count-indicator dropdown-toggle d-flex align-items-center justify-content-center notification-dropdown" id="notificationDropdown" href="#" data-toggle="dropdown">
              <i class="mdi mdi-bell mx-0"></i>
              <span class="count"></span>
            </a>
            <div class="dropdown-menu dropdown-menu-right navbar-dropdown" aria-labelledby="notificationDropdown">
              <p class="mb-0 font-weight-normal float-left dropdown-header">Notificaciones</p>
              <a class="dropdown-item" href="{{ url('datacenter/show') }}">
                <div class="item-thumbnail">
                  <div class="item-icon bg-success">
                    <i class="mdi mdi-mail-ru mx-0"></i>
                  </div>
                </div>
                <div class="item-content">
                  <h6 class="font-weight-normal">Usuarios sin correos 
                      ({{ $_SESSION['notificaciones']->count() }})
                  </h6>
                </div>
              </a>
            </div>
          </li>
          @endif

          <li class="nav-item nav-profile dropdown">
            <a class="nav-link dropdown-toggle" href="#" data-toggle="dropdown" id="profileDropdown">
              <img src="{{ asset('img/user.png') }}" alt="profile"/>
              <span class="nav-profile-name">{{ $_SESSION['nombre'] }}</span>
            </a>
            <div class="dropdown-menu dropdown-menu-right navbar-dropdown" aria-labelledby="profileDropdown">
                @if($_SESSION['rol'] == 1)
                <a class="dropdown-item" href="{{route('user.bitacora')}}">
                  <i class="mdi mdi-bible text-primary"></i>
                  Bitácora
                </a> 
                <a class="dropdown-item" href="{{route('user.index')}}">
                  <i class="mdi mdi-settings text-primary"></i>
                  Cargar Usuarios
                </a>
                @endif
                <a class="dropdown-item" href="{{route('logout')}}">
                  <i class="mdi mdi-logout text-primary"></i>
            		Salir
                </a>
            </div>
          </li>
          
        </ul>
        <button class="navbar-toggler navbar-toggler-right d-lg-none align-self-center" type="button" data-toggle="offcanvas">
          <span class="mdi mdi-menu"></span>
        </button>
      </div>
    </nav>