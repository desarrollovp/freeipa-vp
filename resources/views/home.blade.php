@extends('layouts.app')
@section('title', 'Sistema Freeipa Vicepresidencia')
@section('subtitle', 'Sistema para el registro y contro de usuarios del Freeipa')
@section('css')
{{-- ESTILOS  --}}
@endsection
@section('content')
  <div class="col-md-12 stretch-card">
    <div class="card">
      <div class="card-body">
        <p class="card-title">Inicio</p>
        <hr>
        <div class="table-responsive">
          <div id="chartdiv" class="text-center" >
            <img src="{{ asset('/img/vicelogo.png') }}" style="width: 60%; height: 50vh; margin-top: 20px;">
          </div>
        </div>
      </div>
    </div>
  </div>
@endsection
@section('js')
{{-- SCRIPTS  --}}
@endsection