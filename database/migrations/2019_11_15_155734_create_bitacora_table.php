<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateBitacoraTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('action', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->string('option');
        });

        Schema::create('bitacora', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->string('user_end');
            $table->integer('user_id')->unsigned();
            $table->foreign('user_id')->references('id')->on('users');
            $table->integer('action_id')->unsigned();
            $table->foreign('action_id')->references('id')->on('action');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('bitacora');
        Schema::dropIfExists('action');
    }
}
