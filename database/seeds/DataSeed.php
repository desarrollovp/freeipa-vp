<?php

use Illuminate\Database\Seeder;

class DataSeed extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $roles = array(
            'Administrador', 
            'RRHH',
            'Centro de Datos',

        );

        foreach($roles as $r){
            DB::table('roles')->insert([
            'option' => $r,
            ]);    
        }
        $users = array(

            array('1', 'jlacruz', '1')
        );

        foreach($users as $r){
            DB::table('users')->insert([
            'id' => $r[0],
            'user' => $r[1],
            'rol_id' => $r[2],
            ]);    
        }

        $direcciones = array(
            array('Oficina de Atención al Pueblo','a.ciudadano'),
            array('Oficina de Gestión Administrativa','administracion'),
            array('Dirección General de Articulación con Poderes Públicos y Escalas Territoriales de Gobierno','articulacionterritorial'),
            array('Oficina de Auditoría Interna','auditoria'),
            array('Oficina Administrativa de la Secretaría Ejecutiva del Consejo Nacional de Derechos Humanos','cnddhh'),
            array('Consultoría Jurídica','consultoria'),
            array('Oficina de Gestión Comunicacional','d.comunicaciones'),
            array('Dirección General del Despacho de la Vicepresidencia de la República','d.general'),
            array('Oficina de Seguridad Integral','d.seguridad'),
            array('Dirección General de Delegaciones e Instrucciones Presidenciales','delegaciones'),
            array('Despacho de la Vicepresidenta Ejecutiva','despacho'),
            array('Oficina de Planificación y Presupuesto','planificacionypresupuesto'),
            array('Oficina de Gestión Humana','rrhh'),
            array('Dirección General de Seguimiento y Evaluación de Políticas Públicas','seguimientoycontrol'),
            array('Oficina de Tecnología de la Información y Comunicación','tecnologia'),
        );

        foreach($direcciones as $k){
            DB::table('addresses')->insert([
            'name' => $k[0],
            'conversion' => $k[1],
            ]);    
        }

        $tipoEmpleado = array(
            'Obrero Fijo', 
            'Obrera Fija', 
            'Obrero Contratado', 
            'Obrera Contratada',
            'Empleado Fijo',
            'Empleada Fija',
            'Empleado Contratado',
            'Empleada Contratada',
            'Empleado Fijo Alto Nivel',
            'Empleada Fija Alto Nivel',
            'Comisión de Servicio',
            'Honorarios Profesionales',
            'Jubilados',
            'Pensionados',
        );

        foreach($tipoEmpleado as $t){
            DB::table('tipo_empleado')->insert([
            'opcion' => $t,
            ]);    
        }
        $cargo = array(
            'Vicepresidente Ejecutivo', 
            'Vicepresidenta Ejecutiva', 
            'Auditor Interno', 
            'Auditora Interna',
            'Director General',
            'Directora General',
            'Director de Línea',
            'Directora de Línea',
            'Asistente al Vicepresidencte',
            'Asistente a la Vicepresidenta',
            'Coordinador',
            'Coordinadora',
            'Jefe de Unidad',
            'Jefa de Unidad',
            'Jefe de la Secretaría Privada',
            'Jefa de la Secretaría Privada',
            'Secretario Privado del Vicepresidente',
            'Secretaria Privada de la Vicepresidenta',
            'Asistente de Coordinador',
            'Responsable del Área de Seguridad Integral',
            'Asesor',
            'Especialista',
            'Especialista Asistente',
            'Especialista Jurídico',
            'Auditor Principal',
            'Auditora Principal',
            'Auditor Asistente',
            'Auditora  Asistente',
            'Especialista en Comunicación Social',
            'Especialista en Relaciones Públicas',
            'Administrador Principal',
            'Administradora Principal',
            'Especialista de Compras',
            'Especialista de Bienes y Servicios',
            'Especialista en Recursos Humanos',
            'Contador  Principal',
            'Contadora Principal',
            'Especialista en Informática',
            'Oficinista Asistente',
            'Secretaria Ejecutiva',
            'Secretaria Principal',
            'Secretaria Asistente',
            'Recepcionista del Despacho',
            'Archivista',
            'Archivista Asistente',
            'Asistente en Informática',
            'Asistente en Administración',
            'Asistente en Bienes y Servicios',
            'Especialista en Finanzas',
            'Especialista en Presupuesto',
            'Docente',
            'Auxiliar de Preescolar',
            'Tecnico Administrativo',
            'Analista',
            'Odontologo',
            'Paramedico',
            'Nutricionista',
            'Enfermera',
            'Enfermero',
            'Medico Integral',
            'Psicopedagoga',
            'Analista de Recursos Humanos',
            'Analista de Personal',
            'Camarografo',
            'Fotografo',
            'Analista',
            'Protocolo',
            'Supervisor',
            'Supervisor de Servicios Internos',
            'Supervisor de Cocina',
            'Aseador',
            'Aseadora',
            'Mensajero',
            'Mensajera',
            'Mensajero Motorizado',
            'Ayudante de Servicios de Cocina',
            'Ayudante de Servicios Generales',
            'Barbero',
            'Barbera',
            'Receptor Informador',
            'Ayudante de Almacen',
            'Chofer',
            'Conductor',
            'Cocinero',
            'Cocinera',
            'Auxiliar de Servicios de Oficina',
            'Auxiliar de Servicios de Cocina',
            'Mesonero',
            'Vigilante',
            'Electromecanico',
            'Mecanico Automotriz',
            'Escolta',
            'Electrecista',
            'Empleado Contratado',
            'Empleada Contratada',
            'Obrero Contratado',
            'Obrera Contratada',
        );

        foreach($cargo as $car){
            DB::table('cargo')->insert([
            'opcion' => $car,
            ]);    
        }
        $action = array(
            'Registro usuario', 
            'Modifico usuario',
            'Creación de correo',
            'Deshabilitó usuario',
            'Habilitó usuario',
            'Actualizó correo jubilado',
            'Creación de analista',
        );
        foreach($action as $r){
            DB::table('action')->insert([
            'option' => $r,
            ]);    
        }

    }
}
