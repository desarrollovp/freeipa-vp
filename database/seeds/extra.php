<?php

use Illuminate\Database\Seeder;

class extra extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::table('tipo_empleado')->insert([
            'opcion' => 'Comisión de Servicio'
            ]); 

        DB::table('cargo')->insert([
            'opcion' => 'Comisión de Servicio'
            ]); 
    }
}
